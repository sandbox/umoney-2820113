<?php

require_once(__DIR__ . "/../config.php");
require_once(__DIR__ . "/../util/Http.php");
require_once(__DIR__ . "/../util/Cache.php");
require_once(__DIR__ . "/ISVService.php");

class ISVClass{

    public static function getSuiteAccessToken($SUITE_KEY,$SUITE_SECRET){
        $suiteTicket = Cache::getSuiteTicket();
        if(!$suiteTicket){
            Log::e("ERROR: suiteTicket not cached,please check the callback url");
            return false;
        }
        $suiteAccessToken = ISVService::getSuiteAccessToken($suiteTicket,$SUITE_KEY,$SUITE_SECRET);
        return $suiteAccessToken;
    }

    public static function getIsvCorpAccessToken($suiteAccessToken, $corpId, $permanetCode,$SUITE_KEY){
        $key = "dingdingActive_".$corpId;
        $corpAccessToken = ISVService::getIsvCorpAccessToken($suiteAccessToken, $corpId, $permanetCode);
        $status = Cache::getActiveStatus($key);
        if($status<=0&&$corpAccessToken!=""){
            ISVService::activeSuite($suiteAccessToken, $corpId, $permanetCode,$SUITE_KEY);
        }

        ISVService::getAuthInfo($suiteAccessToken, $corpId, $permanetCode,$SUITE_KEY);
        return $corpAccessToken;
    }

    public static function getCorpInfo($corpId,$SUITE_KEY,$SUITE_SECRET){
        $suiteAccessToken = ISVClass::getSuiteAccessToken($SUITE_KEY,$SUITE_SECRET);
        $corpInfo = ISVService::getCorpInfoByCorId($corpId);
        $corpAccessToken = ISVClass::getIsvCorpAccessToken($suiteAccessToken,$corpInfo['corp_id'],$corpInfo['permanent_code'],$SUITE_KEY);
        $corpInfo['corpAccessToken'] = $corpAccessToken;

        return $corpInfo;
    }
}

function check($res)
{
    if ($res->errcode != 0)
    {
        exit("Failed: " . json_encode($res));
    }
}
