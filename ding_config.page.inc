<?php

/**
 * @file
 * Contains ding_config.page.inc.
 *
 * Page callback for Ding config entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Ding config templates.
 *
 * Default template: ding_config.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ding_config(array &$variables) {
  // Fetch DingConfig Entity Object.
  $ding_config = $variables['elements']['#ding_config'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
