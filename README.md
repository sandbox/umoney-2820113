INTRODUCTION
------------
 dingding support dingtalk isv and corp micro-app, include isv app or corp app .

INSTALLATION
------------
No special install steps are necessary to use this module, see https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.

NOTES
-----
This module response to dingding request. Therefore, you need
to own one/more dingding accounts for this module to work.
About how to apply dingding Accounts, please visit dingding development site https://open.dingtalk.com



MAINTAINERS
-----------
Current maintainers:

 * Umoney (https://www.drupal.org/u/umoney)
