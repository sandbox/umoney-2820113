<?php

/**
 * @file
 * Contains ding_message.page.inc.
 *
 * Page callback for Ding message entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Ding message templates.
 *
 * Default template: ding_message.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ding_message(array &$variables) {
  // Fetch DingMessage Entity Object.
  $ding_message = $variables['elements']['#ding_message'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
