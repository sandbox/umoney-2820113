<?php

namespace Drupal\dingding\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dingding\impl\DrupalDingISV;
use Drupal\dingding\impl\JsonResponse;
use Drupal\dingding\impl\RawResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Context\CookiesCacheContext;
use Drupal\Core\Cache\Context\SessionCacheContext;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Session\AccountProxy;


/**
 * Class DingController.
 *
 * @package Drupal\dingding\Controller
 */
class DingController extends ControllerBase {
  const EVT_check_update_suite_url = 1 ;
  const EVT_suite_ticket = 2 ;
  const EVT_tmp_auth_code = 3 ;
  const EVT_change_auth = 4 ;
  const EVT_suite_relieve =5;


  /**
   * Drupal\Core\Cache\Context\CookiesCacheContext definition.
   *
   * @var \Drupal\Core\Cache\Context\CookiesCacheContext
   */
  protected $cacheContextCookies;
  /**
   * Drupal\Core\Cache\Context\SessionCacheContext definition.
   *
   * @var \Drupal\Core\Cache\Context\SessionCacheContext
   */
  protected $cacheContextSession;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(CookiesCacheContext $cache_context_cookies, SessionCacheContext $cache_context_session, RequestStack $request_stack, LoggerChannelFactory $logger_factory, ConfigManager $config_manager, AccountProxy $current_user) {
    $this->cacheContextCookies = $cache_context_cookies;
    $this->cacheContextSession = $cache_context_session;
    $this->requestStack = $request_stack;
    $this->loggerFactory = $logger_factory;
    $this->configManager = $config_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache_context.cookies'),
      $container->get('cache_context.session'),
      $container->get('request_stack'),
      $container->get('logger.factory'),
      $container->get('config.manager'),
      $container->get('current_user')
    );
  }

  /**
   * T1.
   *
   * @return string
   *   Return Hello string.
   */
  public function dingCallback($did) {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $logger->notice('call back content s1 @aa,@bb ', array(
      '@aa' => $did,
      '@bb' => var_export($content, TRUE)
    ));
    if(count($params)==0){
      $response = new JsonResponse('dingding');
      return $response;
    }
    $this->did = $did;
    $signature = $params["signature"];
    $timeStamp = $params["timestamp"];
    $nonce = $params["nonce"];
    $postList = json_decode($content, TRUE);
    $encrypt = $postList['encrypt'];
    $ding = new DrupalDingISV($did);
    $msg = $ding->decrypt($signature, $timeStamp, $nonce, $encrypt);
    $logger->notice('call back content s2  @aa,@bb ', array(
      '@aa' => $did,
      '@bb' => var_export($msg, TRUE)
    ));
    $res = "success";
    if ($msg) {
      $eventMsg = json_decode($msg);
      $eventType = $eventMsg->EventType;
      $evtmap = array(
        self::EVT_check_update_suite_url => 'check_update_suite_url',
        self::EVT_suite_ticket => 'suite_ticket',
        self::EVT_tmp_auth_code => 'tmp_auth_code',
        self::EVT_change_auth => 'change_auth',
        self::EVT_suite_relieve => 'suite_relieve',
      );

      switch (array_search($eventType,$evtmap)){
        case self::EVT_check_update_suite_url:
          $random = $eventMsg->Random;
          $testSuiteKey = $eventMsg->TestSuiteKey;
          $res = $random;
        break;
        case self::EVT_suite_ticket:
          $ding->setSuiteTicket($eventMsg->SuiteTicket);
          $res = "success";
          break;
        case self::EVT_tmp_auth_code:
          $ding->autoActivateSuite($eventMsg->AuthCode);
          $res = "success";
          break;

        case self::EVT_change_auth:
          $res = "success";
          break;

        case self::EVT_suite_relieve:
          $ding->removeCorpInfo($eventMsg->AuthCorpId);
          $res = "success";
          break;

        default:
          $res = "success";
          $logger->notice('call undefined event @aa,@bb ', array(
            '@aa' => $did,
            '@bb' => var_export($eventType, TRUE)
          ));
          break;
      }

      $encryptMsg = "";
      $encryptMsg = $ding->encrypt($signature, $timeStamp, $nonce, $res);
      if ($encryptMsg == 0) {
        $response = new RawResponse($encryptMsg);
        $response->headers->set('Content-Type','application/json');
        return $response;
      }else {
        $response = new JsonResponse('false');
        return $response;
      }
    }
    else {
      $response = new JsonResponse('false');
      return $response;

    }

  }

}
