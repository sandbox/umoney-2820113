<?php

namespace Drupal\dingding\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dingding\impl\DrupalDingCorp;
use Drupal\dingding\impl\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Session\AccountProxy;

/**
 * Class AppController.
 *
 * @package Drupal\dingding\Controller
 */
class AppCorpController extends ControllerBase {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;
  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, ConfigManager $config_manager, LoggerChannelFactory $logger_factory, AccountProxy $current_user) {
    $this->requestStack = $request_stack;
    $this->configManager = $config_manager;
    $this->loggerFactory = $logger_factory;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.manager'),
      $container->get('logger.factory'),
      $container->get('current_user')
    );
  }

  /**
   * T1.
   *
   * @return string
   *   Return Hello string.
   */
  public function t1($name) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: t1 with parameter(s): $name'),
    ];
  }

  public function corp() {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $data = json_decode($content,TRUE);
    $logger->notice('corp @aa',array('@aa'=>var_export($params,TRUE)));
    $ding = new DrupalDingCorp(1);
    $config = $ding->corpConfig();
    $logger->notice('corp config  @aa',array('@aa'=>var_export($config,TRUE)));
    return [
      '#theme' => 'ding_corp',
      '#config' => json_encode($config),
    ];
  }

  public function corppc() {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $data = json_decode($content,TRUE);
    $corpId = $params['corpid'];
    $logger->notice('corppc @aa',array('@aa'=>var_export($params,TRUE)));
    $ding = new DrupalDingCorp(1);
    $config = $ding->corpConfig($corpId);
    return [
      '#theme' => 'ding_corppc',
      '#config' => json_encode($config),
    ];
  }
  public function sendMsg() {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $data = [];
    $items = explode('&', $content);
    foreach ($items as $item) {
      $pair = explode('=', $item);
      $data[$pair[0]] = $pair[1];
    }
    $logger->notice('sendMsg @aa',array('@aa'=>var_export($data,TRUE)));
    $event = $data['event'];
    if($event ==='send_to_conversation')
    {
      $sender = $data['sender'];
      $cid = $data['cid'];
      $content = $data['content'];
      $option = array(
        "sender"=>$sender,
        "cid"=>$cid,
        "msgtype"=>"text",
        "text"=>array("content"=>urldecode($content))
      );

      $ding = new DrupalDingCorp(1);
      $response = $ding->sendToConversation($option);
      $response = new JsonResponse($response, 200);
      $response->addCacheableDependency($request);


    }else if($event ==='get_userinfo'){
      $code = $data["code"];
      $ding = new DrupalDingCorp(1);
      $userInfo = $ding->getUserInfo($code);
      $response = new JsonResponse($userInfo, 200);
      $response->addCacheableDependency($request);
      $response->setMaxAge(120);

    }else {
      $response = new JsonResponse(array("error_code"=>"4000"), 200);
      $response->addCacheableDependency($request);
    }

    return $response;
  }

}
