<?php

namespace Drupal\dingding\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\dingding\impl\DrupalDingISV;
use Drupal\dingding\impl\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Cache\Context\HeadersCacheContext;

/**
 * Class AppController.
 *
 * @package Drupal\dingding\Controller
 */
class AppISVController extends ControllerBase {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;
  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;
  /**
   * Drupal\Core\Render\Renderer definition.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;
  /**
   * Drupal\Core\Cache\Context\HeadersCacheContext definition.
   *
   * @var \Drupal\Core\Cache\Context\HeadersCacheContext
   */
  protected $cacheContextHeaders;
  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, ConfigManager $config_manager, LoggerChannelFactory $logger_factory, AccountProxy $current_user,Renderer $renderer,HeadersCacheContext $cache_context_headers) {
    $this->requestStack = $request_stack;
    $this->configManager = $config_manager;
    $this->loggerFactory = $logger_factory;
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
    $this->cacheContextHeaders = $cache_context_headers;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.manager'),
      $container->get('logger.factory'),
      $container->get('current_user'),
      $container->get('renderer'),
      $container->get('cache_context.headers')
    );
  }

  /**
   * T1.
   *
   * @return string
   *   Return Hello string.
   */
  public function t1($name) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: t1 with parameter(s): $name'),
    ];
  }
  public function isv() {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $data = json_decode($content,TRUE);
    $corpId = $params['corpid'];
    $logger->notice('isv @aa',array('@aa'=>var_export($params,TRUE)));
    $ding = new DrupalDingISV(1);
    $config = $ding->isvConfig($corpId);
    $build = [
      '#theme' => 'ding_isv',
      '#config' => json_encode($config),
      '#cache'=>['max-age'=>0],
    ];
    return $build;
  }

  public function isvpc() {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $data = json_decode($content,TRUE);
    $corpId = $params['corpid'];
    $logger->notice('isvpc @aa',array('@aa'=>var_export($params,TRUE)));
    $ding = new DrupalDingISV(1);
    $config = $ding->isvConfig($corpId);
    return [
      '#theme' => 'ding_isvpc',
      '#config' => json_encode($config),
    ];
  }

  public function sendMsg() {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->loggerFactory->get('dingding');
    $data = [];
    $items = explode('&', $content);
    foreach ($items as $item) {
      $pair = explode('=', $item);
      $data[$pair[0]] = $pair[1];
    }
    $logger->notice('sendMsg @aa',array('@aa'=>var_export($data,TRUE)));
    $event = $data['event'];
    if($event ==='send_to_conversation')
    {
      $sender = $data['sender'];
      $cid = $data['cid'];
      $content = $data['content'];
      $corpId = $data['corpId'];
      $option = array(
        "sender"=>$sender,
        "cid"=>$cid,
        "msgtype"=>"text",
        "text"=>array("content"=>urldecode($content))
      );

      $ding = new DrupalDingISV(1);
      $resp = $ding->sendToConversation($corpId,$option);
      $response = new JsonResponse($resp, 200);
      $response->getCacheableMetadata()->setCacheMaxAge(0);


    }else if($event ==='get_userinfo'){
      $corpId = $data['corpId'];
      $code = $data["code"];
      $ding = new DrupalDingISV(1);
      $userInfo = $ding->getUserInfo($corpId,$code);
      $response = new JsonResponse($userInfo, 200);
      $response->getCacheableMetadata()->setCacheMaxAge(0);
      $response->setMaxAge(120);

    }else if($event ==='get_config'){
      $corpId = $data['corpId'];
      $ding = new DrupalDingISV(1);
      $config = $ding->isvConfig($corpId);
      $response = new JsonResponse($config, 200);
      $response->getCacheableMetadata()->setCacheMaxAge(0);
      $response->setMaxAge(120);

    }else {
      $response = new JsonResponse(array("error_code"=>"4000"), 200);
      $response->addCacheableDependency($request);
    }

    return $response;
  }

}
