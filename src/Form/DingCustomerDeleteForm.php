<?php

namespace Drupal\dingding\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ding customer entities.
 *
 * @ingroup dingding
 */
class DingCustomerDeleteForm extends ContentEntityDeleteForm {


}
