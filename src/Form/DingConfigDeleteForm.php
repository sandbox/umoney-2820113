<?php

namespace Drupal\dingding\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ding config entities.
 *
 * @ingroup dingding
 */
class DingConfigDeleteForm extends ContentEntityDeleteForm {


}
