<?php

namespace Drupal\dingding\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ding message entities.
 *
 * @ingroup dingding
 */
class DingMessageDeleteForm extends ContentEntityDeleteForm {


}
