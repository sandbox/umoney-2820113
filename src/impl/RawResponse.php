<?php

/**
 * @file
 * Contains \Drupal\Core\Render\HtmlResponse.
 */

namespace Drupal\dingding\impl;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\CacheableResponseTrait;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

/**
 * A response that contains and can expose cacheability metadata and attachments.
 *
 * Supports Drupal's caching concepts: cache tags for invalidation and cache
 * contexts for variations.
 *
 * Supports Drupal's idea of #attached metadata: libraries, settings, http_header and html_head.
 *
 * @see \Drupal\Core\Cache\CacheableResponse
 * @see \Drupal\Core\Render\AttachmentsInterface
 * @see \Drupal\Core\Render\AttachmentsTrait
 */
class RawResponse extends Response implements CacheableResponseInterface {

  use CacheableResponseTrait;
  public function __construct($data = NULL, $status = 200, $headers = array()) {
    parent::__construct($data, $status, $headers);
  }

  /**
   * {@inheritdoc}
   */
  public function setContent($content) {
    $this->content =  $content;

    return $this;
  }

}
