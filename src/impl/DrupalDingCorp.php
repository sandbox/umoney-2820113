<?php

namespace Drupal\dingding\impl;
//define('OAPI_HOST', 'https://oapi.dingtalk.com');
////Suite
//define('CREATE_SUITE_KEY', 'suite4xxxxxxxxxxxxxxx');
//define('SUITE_KEY', 'suiteqetsicctmceqjdqz');
//define('SUITE_SECRET', 'rxbHDpLjyCoJMbuGMMPDpBkJpwYrdtBKsLxWdzZlABLUlOwO4fa3A74SXALZizth');
//define('TOKEN', 'zerogame04');
//define('APPID', '2138');
//define('ENCODING_AES_KEY', 'yd8hg86eo8mx8qmop4g94fphmk2popkr64c7vijl0iq');
require_once dirname(__DIR__) . '/../lib/corp/config.php';
require_once dirname(__DIR__) . '/../lib/corp/util/Log.php';
require_once dirname(__DIR__) . '/../lib/corp/util/Cache.php';
require_once dirname(__DIR__) . '/../lib/corp/api/Auth.php';
require_once dirname(__DIR__) . '/../lib/corp/api/User.php';
require_once dirname(__DIR__) . '/../lib/corp/api/Message.php';

use Drupal\dingding\Entity\DingConfig;
use DingtalkCrypt as corpDingtalkCrypt;
use Cache as corpCache;
use Activate as isvActivate;
use Auth as corpAuth;
use User as corpUser;
use Message as  corpMessage;
class DrupalDingCorp {
  protected $nid;
  protected $config;
  protected $options;
  public  $errCode;
  public function __construct($nid,$options=NULL,$content=NULL)
  {
    if($nid)
    {
      $this->nid = $nid;
      $config = DingConfig::Load($nid);
      $this->options = array(
        'token'=>$config->getToken(), //填写你设定的key
        'encodingaeskey'=>$config->getAESKey(), //填写加密用的EncodingAESKey
        'appid'=>$config->getAppid(), //填写高级调用功能的app id
        'suitesecret'=>$config->getSuiteSecret(), //填写高级调用功能的密钥
        'suitekey'=>$config->getSuiteKey(), //填写高级调用功能的密钥
        'corpid'=>$config->getCorpid(), //填写高级调用功能的app id
        'agentid'=>$config->getAgentid(), //填写高级调用功能的app id
      );
      $this->config = $config;
    }
  }
  public function decrypt($signature, $timeStamp, $nonce, $encrypt){
    $crypt = new corpDingtalkCrypt($this->options['token'], $this->options['encodingaeskey'], $this->options['suitekey']);

    $msg = '';
    $errCode = $crypt->DecryptMsg($signature, $timeStamp, $nonce, $encrypt, $msg);
    return $msg;
  }

  public function encrypt($signature, $timeStamp, $nonce, $res){
    $crypt = new corpDingtalkCrypt($this->options['token'], $this->options['encodingaeskey'], $this->options['suitekey']);

    $msg = '';
    $errCode = $crypt->EncryptMsg($res, $timeStamp, $nonce, $msg);
    return $msg;
  }
//  public function setSuiteTicket($ticket) {
//    corpCache::setSuiteTicket($ticket);
//  }
//
//  public function autoActivateSuite($tmpAuthCode) {
//    corpActivate::autoActivateSuite($tmpAuthCode,$this->options['suitekey'],$this->options['suitesecret']);
//  }
//  public function removeCorpInfo($corpId){
//    ISVService::removeCorpInfo($corpId);
//  }
//
//  public function isvConfig($corpId){
//    return Auth::isvConfig($corpId,$this->config->getAppid(),$this->config->getSuiteKey(),$this->config->getSuiteSecret());
//  }

  public function corpConfig(){
    return corpAuth::corpConfig($this->config->getCorpId(),$this->config->getAgentid(),$this->config->getCorpSecret());
  }
  public function getUserInfo($code){
    $accessToken = corpAuth::getAccessToken($this->config->getCorpId(),$this->config->getCorpSecret());
    $userInfo = corpUser::getUserInfo($accessToken, $code);
    return $userInfo;
  }
  public function sendToConversation($option){
      $corpId = $this->config->getCorpid();
      $appSecret = $this->config->getCorpSecret();
      $accessToken = corpAuth::getAccessToken($corpId,$appSecret);
      $response = corpMessage::sendToConversation($accessToken, $option);
    return $response;

  }

}