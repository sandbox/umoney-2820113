<?php

namespace Drupal\dingding;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ding customer entity.
 *
 * @see \Drupal\dingding\Entity\DingCustomer.
 */
class DingCustomerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\dingding\Entity\DingCustomerInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ding customer entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ding customer entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ding customer entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ding customer entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ding customer entities');
  }

}
