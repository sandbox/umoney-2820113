<?php

namespace Drupal\dingding;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ding config entity.
 *
 * @see \Drupal\dingding\Entity\DingConfig.
 */
class DingConfigAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\dingding\Entity\DingConfigInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ding config entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ding config entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ding config entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ding config entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ding config entities');
  }

}
