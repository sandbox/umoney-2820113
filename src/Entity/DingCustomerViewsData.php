<?php

namespace Drupal\dingding\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ding customer entities.
 */
class DingCustomerViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ding_customer']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Ding customer'),
      'help' => $this->t('The Ding customer ID.'),
    );

    return $data;
  }

}
