<?php

namespace Drupal\dingding\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ding config entities.
 *
 * @ingroup dingding
 */
interface DingConfigInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Ding config name.
   *
   * @return string
   *   Name of the Ding config.
   */
  public function getName();

  /**
   * Sets the Ding config name.
   *
   * @param string $name
   *   The Ding config name.
   *
   * @return \Drupal\dingding\Entity\DingConfigInterface
   *   The called Ding config entity.
   */
  public function setName($name);

  /**
   * Gets the Ding config creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ding config.
   */
  public function getCreatedTime();

  /**
   * Sets the Ding config creation timestamp.
   *
   * @param int $timestamp
   *   The Ding config creation timestamp.
   *
   * @return \Drupal\dingding\Entity\DingConfigInterface
   *   The called Ding config entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ding config published status indicator.
   *
   * Unpublished Ding config are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ding config is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ding config.
   *
   * @param bool $published
   *   TRUE to set this Ding config to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\dingding\Entity\DingConfigInterface
   *   The called Ding config entity.
   */
  public function setPublished($published);

}
