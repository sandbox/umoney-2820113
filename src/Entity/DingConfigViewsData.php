<?php

namespace Drupal\dingding\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ding config entities.
 */
class DingConfigViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ding_config']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Ding config'),
      'help' => $this->t('The Ding config ID.'),
    );

    return $data;
  }

}
