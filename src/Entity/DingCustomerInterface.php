<?php

namespace Drupal\dingding\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ding customer entities.
 *
 * @ingroup dingding
 */
interface DingCustomerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Ding customer name.
   *
   * @return string
   *   Name of the Ding customer.
   */
  public function getName();

  /**
   * Sets the Ding customer name.
   *
   * @param string $name
   *   The Ding customer name.
   *
   * @return \Drupal\dingding\Entity\DingCustomerInterface
   *   The called Ding customer entity.
   */
  public function setName($name);

  /**
   * Gets the Ding customer creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ding customer.
   */
  public function getCreatedTime();

  /**
   * Sets the Ding customer creation timestamp.
   *
   * @param int $timestamp
   *   The Ding customer creation timestamp.
   *
   * @return \Drupal\dingding\Entity\DingCustomerInterface
   *   The called Ding customer entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ding customer published status indicator.
   *
   * Unpublished Ding customer are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ding customer is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ding customer.
   *
   * @param bool $published
   *   TRUE to set this Ding customer to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\dingding\Entity\DingCustomerInterface
   *   The called Ding customer entity.
   */
  public function setPublished($published);

}
