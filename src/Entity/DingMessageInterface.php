<?php

namespace Drupal\dingding\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ding message entities.
 *
 * @ingroup dingding
 */
interface DingMessageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Ding message name.
   *
   * @return string
   *   Name of the Ding message.
   */
  public function getName();

  /**
   * Sets the Ding message name.
   *
   * @param string $name
   *   The Ding message name.
   *
   * @return \Drupal\dingding\Entity\DingMessageInterface
   *   The called Ding message entity.
   */
  public function setName($name);

  /**
   * Gets the Ding message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ding message.
   */
  public function getCreatedTime();

  /**
   * Sets the Ding message creation timestamp.
   *
   * @param int $timestamp
   *   The Ding message creation timestamp.
   *
   * @return \Drupal\dingding\Entity\DingMessageInterface
   *   The called Ding message entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ding message published status indicator.
   *
   * Unpublished Ding message are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ding message is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ding message.
   *
   * @param bool $published
   *   TRUE to set this Ding message to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\dingding\Entity\DingMessageInterface
   *   The called Ding message entity.
   */
  public function setPublished($published);

}
