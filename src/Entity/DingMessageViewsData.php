<?php

namespace Drupal\dingding\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ding message entities.
 */
class DingMessageViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ding_message']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Ding message'),
      'help' => $this->t('The Ding message ID.'),
    );

    return $data;
  }

}
