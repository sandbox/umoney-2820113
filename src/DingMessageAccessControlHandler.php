<?php

namespace Drupal\dingding;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ding message entity.
 *
 * @see \Drupal\dingding\Entity\DingMessage.
 */
class DingMessageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\dingding\Entity\DingMessageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ding message entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ding message entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ding message entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ding message entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ding message entities');
  }

}
