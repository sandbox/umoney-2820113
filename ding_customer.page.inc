<?php

/**
 * @file
 * Contains ding_customer.page.inc.
 *
 * Page callback for Ding customer entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Ding customer templates.
 *
 * Default template: ding_customer.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ding_customer(array &$variables) {
  // Fetch DingCustomer Entity Object.
  $ding_customer = $variables['elements']['#ding_customer'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
